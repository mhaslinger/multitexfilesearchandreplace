﻿using System.Collections.Generic;
using System.Linq;
using MultiTexFileSearchAndReplace.Core.Objects;

namespace MultiTexFileSearchAndReplace.Core
{
    internal class Tokenizer
    {
        public static List<Token> CreateToken(string fullText)
        {
            return fullText.Split(' ').Select(s => new Token(s)).ToList();
        }
    }
}