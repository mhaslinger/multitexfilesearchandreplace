﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using MultiTexFileSearchAndReplace.Core.Objects;

namespace MultiTexFileSearchAndReplace.Core
{
    internal static class Extensions
    {
        internal static Statistics Sum(this IEnumerable<Statistics> source)
        {
            var stat = new Statistics();
            stat = source.Aggregate(stat, (current, statistic) => current + statistic);
            return stat;
        }

        internal static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }

        internal static string FirstCharToUpper(this string input)
        {
            if (String.IsNullOrEmpty(input))
            {
                throw new ArgumentException("input");
            }
            return input.First().ToString(CultureInfo.CurrentUICulture).ToUpper() + String.Join("", input.Skip(1));
        }

        internal static string Replace(this string str, string oldValue, string newValue, StringComparison comparison)
        {
            var sb = new StringBuilder();
            var previousIndex = 0;
            var index = str.IndexOf(oldValue, comparison);
            while (index != -1)
            {
                sb.Append(str.Substring(previousIndex, index - previousIndex));
                sb.Append(newValue);
                index += oldValue.Length;

                previousIndex = index;
                index = str.IndexOf(oldValue, index, comparison);
            }
            sb.Append(str.Substring(previousIndex));
            return sb.ToString();
        }

        internal static string[] RemoveEmptyStrings(this string[] array)
        {
            var list = new List<string>(array.Length);
            list.AddRange(array.Where(s => !string.IsNullOrEmpty(s)));
            return list.ToArray();
        }
    }
}