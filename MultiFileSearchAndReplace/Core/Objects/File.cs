﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MultiTexFileSearchAndReplace.Core.Objects
{
    internal class File
    {
        internal File(List<Token> token, string fullPath, string searchTerm)
        {
            this.FullPath = fullPath;
            this.Name = Path.GetFileNameWithoutExtension(this.FullPath);
            this.Token = token;
            this._createBlocks(searchTerm);
            this.Statistics = new Statistics();
        }

        internal List<Token> Token { get; private set; }
        internal IEnumerable<Block> Blocks { get; private set; }
        internal string FullPath { get; set; }
        internal string Name { get; set; }
        internal Statistics Statistics { get; set; }

        internal string Text
        {
            get
            {
                var sb = new StringBuilder(this.Token.Count*2);
                foreach (var token in this.Token)
                {
                    sb.Append(token.Term);
                    sb.Append(" ");
                }
                //remove last whitespace
                sb.Length--;
                return sb.ToString();
            }
        }

        private void _createBlocks(string searchTerm)
        {
            var bc = new BlockCreator(this.Token, searchTerm, this);
            this.Blocks = bc.GetBlocks();
        }

        internal void AppendCopyToFileName()
        {
            var dir = Path.GetDirectoryName(this.FullPath);
            var file = Path.GetFileNameWithoutExtension(this.FullPath) + "_copy";
            var extension = Path.GetExtension(this.FullPath);
            if (dir == null || extension == null)
            {
                throw new InvalidOperationException("error retrieving file names");
            }
            this.FullPath = Path.Combine(dir, file + extension);
        }
    }
}