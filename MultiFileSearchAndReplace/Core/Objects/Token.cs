﻿using System;
using System.Globalization;
using System.Linq;

namespace MultiTexFileSearchAndReplace.Core.Objects
{
    internal class Token
    {
        internal Token(string term)
        {
            this.Id = Guid.NewGuid();
            this.Term = term;
        }

        internal Guid Id { get; private set; }
        internal string Term { get; private set; }

        internal void ReplaceInTerm(string oldValue, string newValue)
        {
            if (this.Term == null)
            {
                return;
            }
            var nV = (this.Term.StartsWith(oldValue, true, CultureInfo.CurrentUICulture) && char.IsUpper(this.Term.First())) ? newValue.FirstCharToUpper() : newValue.ToLower();
            this.Term = this.Term.Replace(oldValue, nV, StringComparison.OrdinalIgnoreCase);
        }
    }
}