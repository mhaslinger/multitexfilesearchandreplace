﻿namespace MultiTexFileSearchAndReplace.Core.Objects
{
    public class ProcessStatisticsViewModel
    {
        internal ProcessStatisticsViewModel(File file)
        {
            this.Name = file.Name;
            this.Found = file.Statistics.Found;
            this.Replaced = file.Statistics.Replaced;
        }

        public string Name { get; set; }
        public ulong Found { get; set; }
        public ulong Replaced { get; set; }
    }
}