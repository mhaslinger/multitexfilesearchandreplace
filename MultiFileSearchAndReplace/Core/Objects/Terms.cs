﻿namespace MultiTexFileSearchAndReplace.Core.Objects
{
    internal struct Terms
    {
        internal string SearchTerm { get; set; }
        internal string ReplaceTerm { get; set; }
    }
}