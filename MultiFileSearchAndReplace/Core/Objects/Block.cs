﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Controls;
using System.Windows.Documents;

namespace MultiTexFileSearchAndReplace.Core.Objects
{
    internal class Block
    {
        internal Block(List<Token> tokenBefore, List<Token> tokenAfter, Token token, File file)
        {
            this.Token = token;
            this.TokenBefore = tokenBefore;
            this.TokenAfter = tokenAfter;
            this._file = file;
        }

        internal List<Token> TokenBefore { get; private set; }
        internal List<Token> TokenAfter { get; private set; }
        internal Token Token { get; private set; }
        private File _file { get; set; }
        private bool _replaced { get; set; }

        internal void GetText(TextBlock textBlock, string highlightTerm)
        {
            textBlock.Text = string.Empty;
            textBlock.Inlines.Clear();
            var sbSize = this.TokenAfter.Count + this.TokenBefore.Count + 1;
            var sb = new StringBuilder(sbSize);
            var concat = new Action<Token>(t => sb.Append(t.Term).Append(" "));
            this.TokenBefore.ForEach(concat);
            textBlock.Inlines.Add(new Run(sb.ToString()));
            sb.Clear();
            var tokenParts = Regex.Split(this.Token.Term, highlightTerm, RegexOptions.IgnoreCase).RemoveEmptyStrings();
            var highlightText = this.Token.Term.StartsWith(highlightTerm, true, CultureInfo.CurrentUICulture) && char.IsUpper(this.Token.Term.First()) ? highlightTerm.FirstCharToUpper() : highlightTerm;
            switch (tokenParts.Length)
            {
                case 2:
                {
                    textBlock.Inlines.Add(new Run(tokenParts[0]));
                    textBlock.Inlines.Add(new Bold(new Run(highlightText)));
                    textBlock.Inlines.Add(new Run(tokenParts[1] + " "));
                }
                    break;
                case 1:
                {
                    if (this.Token.Term.StartsWith(highlightTerm, true, CultureInfo.CurrentUICulture))
                    {
                        textBlock.Inlines.Add(new Bold(new Run(highlightText)));
                        textBlock.Inlines.Add(new Run(tokenParts[0] + " "));
                    }
                    else
                    {
                        textBlock.Inlines.Add(new Run(tokenParts[0]));
                        textBlock.Inlines.Add(new Bold(new Run(highlightText)));
                    }
                }
                    break;
                case 0:
                {
                    var repeat = Regex.Matches(this.Token.Term, highlightText).Count;
                    if (repeat > 1)
                    {
                        for (var i = 0; i < (repeat - 1); i++)
                        {
                            textBlock.Inlines.Add(new Bold(new Run(highlightText)));
                        }
                    }
                    textBlock.Inlines.Add(new Bold(new Run(highlightText + " ")));
                }
                    break;
            }
            this.TokenAfter.ForEach(concat);
            if (sb.Length > 0)
            {
                //remove the last whitespace
                sb.Length--;
            }
            textBlock.Inlines.Add(new Run(sb.ToString()));
        }

        internal void Replace(string oldValue, string newValue)
        {
            if (this._replaced)
            {
                return;
            }
            this.Token.ReplaceInTerm(oldValue, newValue);
            this._file.Statistics.Replaced++;
            this._replaced = true;
        }
    }
}