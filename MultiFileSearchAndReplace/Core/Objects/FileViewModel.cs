﻿namespace MultiTexFileSearchAndReplace.Core.Objects
{
    public class FileViewModel
    {
        public string Name { get; set; }
        public string FullPath { get; set; }
    }
}