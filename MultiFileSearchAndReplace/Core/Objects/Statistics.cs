﻿namespace MultiTexFileSearchAndReplace.Core.Objects
{
    internal class Statistics
    {
        internal ulong Found { get; set; }
        internal ulong Replaced { get; set; }

        public static Statistics operator +(Statistics statisticsLeft, Statistics statisticsRight)
        {
            var newStat = new Statistics {Found = statisticsLeft.Found + statisticsRight.Found, Replaced = statisticsLeft.Replaced + statisticsRight.Replaced};
            return newStat;
        }
    }
}