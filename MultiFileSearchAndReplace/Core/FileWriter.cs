﻿using System;
using System.IO;
using System.Windows;
using File = MultiTexFileSearchAndReplace.Core.Objects.File;

namespace MultiTexFileSearchAndReplace.Core
{
    internal class FileWriter
    {
        public static void WriteFile(File file)
        {
            if (System.IO.File.Exists(file.FullPath))
            {
                System.IO.File.Delete(file.FullPath);
            }
            try
            {
                using (var stream = System.IO.File.OpenWrite(file.FullPath))
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write(file.Text);
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Error");
            }
        }
    }
}