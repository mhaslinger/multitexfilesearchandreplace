﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MultiTexFileSearchAndReplace.Core.Objects;

namespace MultiTexFileSearchAndReplace.Core
{
    internal class FileLoader
    {
        internal static Task<List<File>> LoadFilesAsync(List<FileViewModel> fileList, string searchTerm)
        {
            return Task.Run(() => LoadFiles(fileList, searchTerm));
        }

        internal static List<File> LoadFiles(List<FileViewModel> fileList, string searchTerm)
        {
            var list = new ConcurrentBag<File>();
            var res = Parallel.ForEach(fileList, fl =>
                                                 {
                                                     var fullText = System.IO.File.ReadAllText(fl.FullPath);
                                                     var token = Tokenizer.CreateToken(fullText);
                                                     var file = new File(token, fl.FullPath, searchTerm);
                                                     list.Add(file);
                                                 });
            var retList = new List<File>(list.Count);
            if (res.IsCompleted)
            {
                retList.AddRange(list);
                retList = retList.OrderBy(f => f.Name).ToList();
            }
            return retList;
        }
    }
}