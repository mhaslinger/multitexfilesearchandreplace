﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MultiTexFileSearchAndReplace.Core.Objects;

namespace MultiTexFileSearchAndReplace.Core
{
    internal class ReplaceHandler : IDisposable
    {
        private List<File> _files;

        internal IEnumerable<Block> Blocks
        {
            get
            {
                foreach (var file in this._files)
                {
                    var blockCount = 0UL;
                    foreach (var block in file.Blocks)
                    {
                        yield return block;
                        blockCount++;
                    }
                    file.Statistics.Found = blockCount;
                }
            }
        }

        internal IEnumerable<Statistics> Statistics
        {
            get { return this._files.Select(f => f.Statistics); }
        }

        internal List<ProcessStatisticsViewModel> ProcessStatistics
        {
            get { return this._files.Select(f => new ProcessStatisticsViewModel(f)).ToList(); }
        }

        public void Dispose()
        {
            this._files.Clear();
        }

        internal async Task Setup(List<FileViewModel> fileList, string searchTerm)
        {
            this._files = await FileLoader.LoadFilesAsync(fileList, searchTerm);
        }

        public bool AddCopyExtensionToFileNames()
        {
            try
            {
                this._files.ForEach(f => f.AppendCopyToFileName());
            }
            catch (InvalidOperationException e)
            {
                MessageBox.Show(e.Message, "Error");
                return false;
            }
            return true;
        }

        internal Task WriteFiles()
        {
            return Task.Run(() => Parallel.ForEach(this._files, FileWriter.WriteFile));
        }
    }
}