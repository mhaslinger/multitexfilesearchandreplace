﻿using System;
using System.Collections.Generic;
using MultiTexFileSearchAndReplace.Core.Objects;

namespace MultiTexFileSearchAndReplace.Core
{
    internal class BlockCreator
    {
        private readonly List<Token> _beforeToken;
        private readonly File _file;
        private readonly string _searchTerm;
        private readonly List<Token> _token;

        internal BlockCreator(List<Token> token, string searchTerm, File file)
        {
            this._beforeToken = new List<Token>(5);
            this._searchTerm = searchTerm;
            this._token = token;
            this._file = file;
        }

        internal IEnumerable<Block> GetBlocks()
        {
            for (var i = 0; i < this._token.Count; i++)
            {
                if (this._token[i].Term.Contains(this._searchTerm, StringComparison.OrdinalIgnoreCase))
                {
                    yield return new Block(this._beforeToken, _getAfterToken(i, this._token), this._token[i], this._file);
                }
                this._addToBeforeBlocks(this._token[i]);
            }
        }

        private static List<Token> _getAfterToken(int i, List<Token> token)
        {
            var diff = (token.Count - 1) - i;
            if (diff == 0)
            {
                return new List<Token>();
            }
            if (diff > 5)
            {
                diff = 5;
            }
            return token.GetRange(i + 1, diff);
        }

        private void _addToBeforeBlocks(Token token)
        {
            if (this._beforeToken.Count == 5)
            {
                this._beforeToken.RemoveAt(0);
            }
            this._beforeToken.Add(token);
        }
    }
}