﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using MultiTexFileSearchAndReplace.Core;
using MultiTexFileSearchAndReplace.Core.Objects;
using Block = MultiTexFileSearchAndReplace.Core.Objects.Block;
using File = System.IO.File;

namespace MultiTexFileSearchAndReplace
{
    public partial class MainWindow
    {
        private readonly ObservableCollection<FileViewModel> _files;
        private readonly ObservableCollection<ProcessStatisticsViewModel> _procStats;
        private readonly ReplaceHandler _replaceHandler;
        private IEnumerator<Block> _enumerator;
        private bool _licenseSwitch;
        private bool _tabControlEnabled;
        private Terms _terms;

        public MainWindow()
        {
            this._files = new ObservableCollection<FileViewModel>();
            this._procStats = new ObservableCollection<ProcessStatisticsViewModel>();
            this._tabControlEnabled = true;
            this._replaceHandler = new ReplaceHandler();
            this.InitializeComponent();
            this.FileListView.ItemsSource = this._files;
            this.ResultListView.ItemsSource = this._procStats;
        }

        private void ButtonHighlightMouseEnter(object sender, MouseEventArgs e)
        {
            var button = (Button) sender;
            button.Background = new SolidColorBrush(Colors.White);
            button.Foreground = new SolidColorBrush(Colors.Black);
            button.BorderBrush = new SolidColorBrush(Colors.White);
        }

        private void SuccessButtonMouseLeave(object sender, MouseEventArgs e)
        {
            var button = (Button) sender;
            button.Background = new SolidColorBrush(Colors.Green);
            button.Foreground = new SolidColorBrush(Colors.White);
            button.BorderBrush = new SolidColorBrush(Colors.Green);
        }

        private void DangerButtonMouseLeave(object sender, MouseEventArgs e)
        {
            var button = (Button) sender;
            button.Background = new SolidColorBrush(Colors.DarkRed);
            button.Foreground = new SolidColorBrush(Colors.White);
            button.BorderBrush = new SolidColorBrush(Colors.DarkRed);
        }

        private void FileListViewDrop(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                return;
            }
            var files = (string[]) e.Data.GetData(DataFormats.FileDrop);
            foreach (var file in from filePath in files
                                 let fileInfo = new FileInfo(filePath)
                                 where fileInfo.Extension.Equals(".tex")
                                 select new FileViewModel {FullPath = filePath, Name = fileInfo.Name})
            {
                this._files.Add(file);
            }
            var sorted = this._files.OrderBy(f => f.Name).ToList();
            this._files.Clear();
            sorted.ForEach(this._files.Add);
        }

        private void FileListViewPreviewMouseWheel(object sender, MouseWheelEventArgs e)
        {
            this.FileListScrollViewer.ScrollToVerticalOffset(this.FileListScrollViewer.VerticalOffset - e.Delta);
            e.Handled = true;
        }

        private void StartButtonClick(object sender, RoutedEventArgs e)
        {
            this.SelectFilesTabItem.IsSelected = true;
        }

        private void SelectFilesNextButtonClick(object sender, RoutedEventArgs e)
        {
            this.SetTermsTabItem.IsSelected = true;
        }

        private void SetTermsNextButtonClick(object sender, RoutedEventArgs e)
        {
            this.StepThroughTabItem.IsSelected = true;
        }

        private void SetTermsBackButtonClick(object sender, RoutedEventArgs e)
        {
            this.StartButtonClick(sender, e);
        }

        private void MainTabControlSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            e.Handled = true;
            if (!this._tabControlEnabled)
            {
                if (this.StepThroughTabItem.IsSelected)
                {
                    return;
                }
                this.StepThroughTabItem.IsSelected = true;
                MessageBox.Show("Please wait for the current operation to finish.", "System Message");
                return;
            }
            if (this.StepThroughTabItem.IsSelected)
            {
                this._prepareSearchAndReplace();
            }
        }

        private void _disableTabControl()
        {
            this._tabControlEnabled = false;
        }

        private void _enableTabControl()
        {
            this._tabControlEnabled = true;
        }

        private async void _prepareSearchAndReplace()
        {
            this.ReplaceBlockTextBlock.Text = string.Empty;
            this.ReplaceBlockTextBlock.Inlines.Clear();
            //this._disableTabControl();
            try
            {
                if (this._validateInputs())
                {
                    this._showLoadingGrid();
                    this._terms = new Terms {ReplaceTerm = this.ReplaceTermTextBox.Text, SearchTerm = this.SearchTermTextBox.Text};
                    this.SearchTermReadonlyBox.Text = this._terms.SearchTerm;
                    this.ReplaceTermReadonlyBox.Text = this._terms.ReplaceTerm;
                    await this._replaceHandler.Setup(this._files.Select(f => f).ToList(), this._terms.SearchTerm);
                    this._enumerator = this._replaceHandler.Blocks.GetEnumerator();
                    var hasOneBlock = this._enumerator.MoveNext();
                    if (hasOneBlock)
                    {
                        this._enumerator.Current.GetText(this.ReplaceBlockTextBlock, this._terms.SearchTerm);
                    }
                    else
                    {
                        this.ReplaceBlockTextBlock.Inlines.Add(new Italic(new Run("Search term not found in supplied files!")));
                        this._enumerator.Dispose();
                    }
                    this._hideLoadingGrid();
                }
                else
                {
                    //this._enableTabControl();
                    this.SelectFilesTabItem.IsSelected = true;
                }
            }
            finally
            {
                //this._enableTabControl();
            }
        }

        private void _hideLoadingGrid()
        {
            this.SearchAndReplaceLoadingGrid.Visibility = Visibility.Hidden;
            this.StepThroughGrid.Visibility = Visibility.Visible;
        }

        private void _showLoadingGrid()
        {
            this.StepThroughGrid.Visibility = Visibility.Hidden;
            this.SearchAndReplaceLoadingGrid.Visibility = Visibility.Visible;
        }

        private bool _validateInputs()
        {
            var fileCountOk = this._files.Count > 0;
            var searchTermOk = !string.IsNullOrEmpty(this.SearchTermTextBox.Text);
            var replaceTermOk = !string.IsNullOrEmpty(this.ReplaceTermTextBox.Text);
            var filesOk = fileCountOk && this._files.All(fileViewModel => File.Exists(fileViewModel.FullPath));
            if (fileCountOk && filesOk && searchTermOk && replaceTermOk)
            {
                return true;
            }
            var msg = "Please check your input:\n\n";
            msg += "File selected: " + (fileCountOk ? "ok" : "no") + "\n";
            msg += "Files exist: " + (filesOk ? "ok" : "no") + "\n";
            msg += "SearchTerm supplied: " + (searchTermOk ? "ok" : "no") + "\n";
            msg += "ReplaceTerm supplied: " + (replaceTermOk ? "ok" : "no");
            MessageBox.Show(msg, "Validation Result");
            return false;
        }

        private void ReplaceNextButtonClick(object sender, RoutedEventArgs e)
        {
            if (this._enumerator == null)
            {
                return;
            }
            var hasNext = this._enumerator.MoveNext();
            if (hasNext)
            {
                this._enumerator.Current.GetText(this.ReplaceBlockTextBlock, this._terms.SearchTerm);
            }
            else
            {
                this._showTotalStats(this._replaceHandler.Statistics);
                this._procStats.Clear();
                this._replaceHandler.ProcessStatistics.ForEach(this._procStats.Add);
                this.SaveTabItem.IsSelected = true;
            }
        }

        private void _showTotalStats(IEnumerable<Statistics> stats)
        {
            var total = stats.Sum();
            var text = "Total\nFound: ";
            text += total.Found;
            text += "\nReplaced: ";
            text += total.Replaced;
            this.OverallStatisticsBlock.Text = text;
        }

        private void ReplaceButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                this._enumerator.Current.Replace(this._terms.SearchTerm, this._terms.ReplaceTerm);
                this._enumerator.Current.GetText(this.ReplaceBlockTextBlock, this._terms.ReplaceTerm);
            }
            catch (NullReferenceException)
            {
                MessageBox.Show("Sorry, I didn't find any occurence.\nI'm afraid I cannot allow you to replace nothing, that would break the universe.", "Error");
            }
        }

        private void DiscardChangesButtonClick(object sender, RoutedEventArgs e)
        {
            this._replaceHandler.Dispose();
            this.StartTabItem.IsSelected = true;
        }

        private async void SaveFilesButtonClick(object sender, RoutedEventArgs e)
        {
            this._showSavingGrid();
            var ok = true;
            if (this.AddCopyToFileNameCheckBox.IsChecked != null && (bool) this.AddCopyToFileNameCheckBox.IsChecked)
            {
                ok = this._replaceHandler.AddCopyExtensionToFileNames();
            }
            if (ok)
            {
                await this._replaceHandler.WriteFiles();
                MessageBox.Show("Files written.", "Info");
            }
            this._hideSavingGrid();
        }

        private void _hideSavingGrid()
        {
            this.SaveLoadingGrid.Visibility = Visibility.Hidden;
            this.SaveMainGrid.Visibility = Visibility.Visible;
        }

        private void _showSavingGrid()
        {
            this.SaveMainGrid.Visibility = Visibility.Hidden;
            this.SaveLoadingGrid.Visibility = Visibility.Visible;
        }

        private void SelectFilesRemoveFileButtonClick(object sender, RoutedEventArgs e)
        {
            var selItem = this.FileListView.SelectedItem;
            if (selItem == null)
            {
                return;
            }
            this._files.Remove((FileViewModel) selItem);
        }

        private void AboutButtonClick(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("(C) marce.at " + DateTime.Now.Year, "Info");
        }

        private void LicenseButtonClick(object sender, RoutedEventArgs e)
        {
            if (!this._licenseSwitch)
            {
                this.LicenseScrollViewer.Visibility = Visibility.Visible;
                this._licenseSwitch = true;
            }
            else
            {
                this.LicenseScrollViewer.Visibility = Visibility.Collapsed;
                this._licenseSwitch = false;
            }
        }
    }
}